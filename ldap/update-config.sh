#!/bin/sh

escape_for_sed() {
    echo $1 | sed 's/[/&\\]/\\\0/g'
}

DN="dc=$(echo $HOST | sed 's/\./,dc=/g')"
CRYPT_SIEADM_PW="{CRYPT}$(echo -n $SIEADM_PASSWORD | mkpasswd -m sha-512 -s)"
CRYPT_AUTO_PW="{CRYPT}$(echo -n $AUTO_PASSWORD | mkpasswd -m sha-512 -s)"

for fn in $(find /container/service/slapd/assets/config/bootstrap/ldif /container/environment/01-custom  -type f); do
    sed -i "s/{{host-dn}}/$(escape_for_sed $DN)/g" "$fn"
    sed -i "s/{{hostname}}/$(escape_for_sed $HOST)/g" "$fn"
    sed -i "s/{{ldap-admin-password}}/$(escape_for_sed $LDAP_ADMIN_PASSWORD)/g" "$fn"
    sed -i "s|{{sieadm-pw-crypt}}|$CRYPT_SIEADM_PW|g" "$fn"
    sed -i "s|{{auto-pw-crypt}}|$CRYPT_AUTO_PW|g" "$fn"
done
