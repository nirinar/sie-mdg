# Conteneur ldap-server

Ce conteneur tient les comptes d'utilisateurs du système. Toutes les composantes du système dirigés aux utilisateurs internes (les membres de l'équipe SIE au ministère) sont connectées à ce conteneur pour la vérification de l'autorisation d'un utilisateur. Pour cette raison un changement du mot de passe d'un utilisateur sera immédiatement effectif dans toutes les composantes et un nouveau compte d'utilisateur sera automatiquement disponible pour l'enregistrement dans toutes les composantes.

La seule exception est Joomla qui n'est pas connecté.

## Configuration

Le fichier `environment/my-env.startup.yaml` permet la configuration de deux mots de passe administratifs et du nom de l’organisation
qui est le propriétaire du registre LDAP. Il est important de ne pas changer les mots de passe contenus ici comme ils sont utilisés par tous les autres composantes. A la place de changer ce fichier on peut reconfigurer le mot de passe administratif du serveur LDAP dans le fichier `.env` au répertoire principal. De cette façon, le changement sera propagé à tous les composantes.

On peut mettre des fichiers LDIF contenant des définitions pour l’initialisation du registre LDAP dans le sous-dossier
`bootstrap/ldif`. Par ex., pour migrer un registre d’un autre serveur, on peut exécuter sur le serveur d’origine (en
réajustant les identifiants) la commande qui exportera tous les groupes et les utilisateurs :

```sh
ldapsearch -Wx -D 'cn=admin,dc=sie-madagascar-madagascar,dc=info' -b 'dc=sie-madagascar-madagascar,dc=info' -LLL '(|(ou:dn:=People)(ou:dn:=Groups))' | tee full.ldif
```

Si le nouveau serveur opère sous une autre adresse il faut remplacer chaque occurrence de la partie principale des
« noms distingués », etc. par le morceau `{{host-dn}}`. Pour l’exemple au-dessus cela serai fait par la commande :

```sh
sed -i 's/dc=sie-madagascar-madagascar,dc=info/{{host-dn}}/g' full.ldif
```

Après, le fichier est prêt pour le mettre dans le sous-répertoire `bootstrap/ldif`.

*Attention* : Les fichiers dans ce répertoire ne seront importés que si le volume du conteneur est vide! C'est normalement le cas pendant le premier lancement du conteneur. Si le conteneur à déjà été lancé une fois sur un ordinateur il faut d'abord supprimer le volume existant par exécuter la commande `docker volume rm shelldocker_ldap-lib shelldocker_ldap-conf` (les noms des volumes doivent être ajustés selon la configuration sur le système). On perd la configuration et les comptes d'utilisateur de l'ancien conteneur dans ce processus.
