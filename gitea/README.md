# Conteneur gitea

[Gitea](https://gitea.io) est un système pour tenir des référentiels de code de source ensemble avec l'historique entière de développement des tâches et des bogues pour chaque référentiel, etc.

Vous trouvez dans ce système toutes les sources à partir desquelles le système a été généré. Ces sources sont disponibles ici comme référence technique et pour assurer la maintenance et le développement futur du système.

Le logiciel est rattaché au service LDAP pour l'enregistrement d'utilisateurs. Tous les utilisateurs membres de l'un des groupes « data_admins » ou « user_admins » peuvent accéder au système. Les membres du groupe « user_admins » ont aussi des droits administratifs pour Gitea.
