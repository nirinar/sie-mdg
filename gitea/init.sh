#!/bin/sh

REPOS="importateurs shell shell-backend shell_docker"

# Wait until Gitea becomes available
until curl -s http://localhost:3000/api/v1/version > /dev/nul 2>&1; do
    sleep 5
done

# Link to LDAP
DN="dc=$(echo $HOST | sed 's/\./,dc=/g')"
su-exec git /app/gitea/gitea admin auth add-ldap --name "SIE LDAP" --security-protocol unencrypted --host ldap-server --port 389 --user-search-base "ou=People,$DN" --user-filter "(&(objectClass=inetOrgPerson)(uid=%s)(|(memberOf=cn=data_admins,ou=Groups,$DN)(memberOf=cn=user_admins,ou=Groups,$DN)))" --admin-filter "(memberOf=cn=user_admins,ou=Groups,$DN)" --username-attribute uid --firstname-attribute givenName --surname-attribute sn --email-attribute mail --bind-dn "cn=admin,$DN" --bind-password "$LDAP_BIND_PASSWORD" --synchronize-users true
# Create organization SIE
SIE_UID=$(curl -u sieadm:$SIEADM_PASSWORD -X POST -H 'Content-type: application/json' -d '{"username":"sie","full_name":"Système d’Information Energétique"}' http://localhost:3000/api/v1/orgs | grep -oE '"id"[[:blank:]]*:[[:blank:]]*[[:digit:]]+' | grep -oE '[[:digit:]]+')

# Migrate repos if a source is available
if test -z "$GIT_SOURCE"; then
    exit 0
fi

for REPO in $(echo $REPOS); do
    curl -u sieadm:$SIEADM_PASSWORD -X POST -H 'Content-type: application/json' -d "{\"clone_addr\":\"$GIT_SOURCE/sie/$REPO.git\",\"private\":false,\"repo_name\":\"$REPO\",\"uid\":$SIE_UID}" http://localhost:3000/api/v1/repos/migrate
done
