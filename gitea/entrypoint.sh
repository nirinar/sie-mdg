#!/bin/sh

# On first run, set up configuration and trigger initialization
if test ! -f /data/gitea/conf/app.ini; then
    mkdir -p /data/git
    mkdir -p /data/gitea/conf
    if test ${HTTPS:-0} -eq 1; then
        PROTOCOL=https://
    else
        PROTOCOL=http://
    fi
    envsubst < /usr/local/share/gitea_docker/app.ini > /data/gitea/conf/app.ini
    chown -R git:git /data/git /data/gitea
    /bin/sh -c "PROTOCOL=$PROTOCOL && /usr/local/bin/init.sh" &
fi

exec su-exec git /app/gitea/gitea web
