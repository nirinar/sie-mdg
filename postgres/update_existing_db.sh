
/* RUN script either by logging into postgres with psql -U postgres -d sie and copy-pasting, or  */ 
/* from the command line, file needs to be created first, with psql -U postgres -d sie -f update_existing_db.sh  * /  

create table etl_matrice_copy as table etl_matrice; 
delete * from etl_matrice_copy; 
CREATE OR REPLACE VIEW warehouse.matrice_complete_copy
AS SELECT etl_matrice_copy.code,
    etl_matrice_copy.mois,
    etl_matrice_copy.valeur,
    etl_matrice_copy.id_societe,
    etl_matrice_copy.centrale,
    codes_matrice.categorie,
    codes_matrice.renouvelable,
    codes_matrice.unite,
    codes_matrice.tension,
    codes_matrice.consommateur,
    centrales.nom AS nom_de_centrale,
    centrales.district_pcode,
    company.name AS nom_de_societe,
    district.name AS nom_de_district,
    district.region_pcode,
    region.name AS nom_de_region,
    codes_matrice.type_energie,
    date_part('year'::text, etl_matrice_copy.mois)::text AS annee,
    region.taille_menage,
    region.taille_menage_rural,
    region.taille_menage_urbain,
    district_population.population,
    COALESCE(centrales_jirama.milieu, 'R'::bpchar) AS milieu,
    'MG-' || region.pcode as region_iso,
    'MG' || region.pcode || district.pcode as district_iso
   FROM warehouse.etl_matrice_copy
     JOIN warehouse.codes_matrice ON etl_matrice_copy.code::text = codes_matrice.code::text
     JOIN warehouse.centrales ON etl_matrice_copy.centrale::text = centrales.code::text AND etl_matrice_copy.id_societe = centrales.societe
     JOIN warehouse.company ON etl_matrice_copy.id_societe = company.id
     JOIN warehouse.district ON centrales.district_pcode::text = district.pcode::text
     JOIN warehouse.region ON district.region_pcode::text = region.pcode::text
     LEFT JOIN warehouse.district_population ON district_population.district_pcode::text = district.pcode::text AND date_part('year'::text, etl_matrice_copy.mois) = district_population.year::double precision
     LEFT JOIN warehouse.centrales_jirama ON centrales.code::text = centrales_jirama.id::text AND centrales.societe = '11556ea8-3b66-11e9-ae4e-0242113dc113'::uuid;

CREATE OR REPLACE VIEW warehouse.region_details_copy
AS SELECT region.pcode,
    region.name,
    ( SELECT sum(dp2.population) AS sum
           FROM warehouse.district_population dp2
             JOIN warehouse.district d2 ON d2.pcode::text = dp2.district_pcode::text AND d2.region_pcode::text = region.pcode::text AND date_part('year'::text, matrice_connexions_p.mois) = dp2.year::double precision) AS population,
    sum(matrice_connexions_p.valeur * region.taille_menage *
        CASE centrales_jirama.milieu
            WHEN 'U'::bpchar THEN 1.7
            ELSE 1.3
        END::double precision) AS desservi,
    sum(matrice_connexions_p.valeur) AS connexions,
    matrice_connexions_p.mois AS date,
    ('MG-'::text || region.pcode::text) AS iso
   FROM warehouse.district
     JOIN warehouse.region ON district.region_pcode::text = region.pcode::text
     LEFT JOIN warehouse.centrales ON centrales.district_pcode::text = district.pcode::text
     LEFT JOIN warehouse.centrales_jirama ON centrales.societe = '11556ea8-3b66-11e9-ae4e-0242113dc113'::uuid AND centrales.code::text = centrales_jirama.id::text
     LEFT JOIN warehouse.etl_matrice_copy matrice_connexions_p ON matrice_connexions_p.centrale::text = centrales.code::text AND matrice_connexions_p.id_societe = centrales.societe AND matrice_connexions_p.code::text = 'CO0202'::text
  GROUP BY region.pcode, region.name, matrice_connexions_p.mois;

ALTER TABLE warehouse.imports                                                                                                                                                                
ADD COLUMN IF NOT EXISTS approving_user varchar,                                                                                                                                                           
ADD COLUMN IF NOT EXISTS starting_user varchar;                                                                                                                                                            

# if these two lines fail, comment them out; CREATE TYPE IF NOT EXISTS does not work                                                                                                                                                                                             
CREATE TYPE step AS ENUM ('check_file', 'calculate_kpi', 'approve', 'import') ;                                                                                                              
CREATE TYPE status AS ENUM ('failed', 'in progress', 'skipped', 'done');                                                                                                                                
CREATE TABLE IF NOT EXISTS warehouse.imports_status (                                                                                                                                        
        id SERIAL,                                                                                                                                                                    
        import_id uuid NOT NULL,                                                                                                                                                             
        "timestamp" timestamp NOT NULL,                                                                                                                                                      
        import_step step NOT NULL, 
        import_status status NOT NULL,
        "user" varchar NULL,   
        result_information varchar NULL, 
        CONSTRAINT pk_imports_status PRIMARY KEY (id), 
        CONSTRAINT fk_imports_status FOREIGN KEY(import_id) REFERENCES warehouse.imports(id)
); 

ALTER TABLE warehouse.imports_status ALTER COLUMN "user" DROP NOT NULL;  
ALTER TABLE imports_status DROP COLUMN id;  
ALTER TABLE imports_status ADD COLUMN id SERIAL PRIMARY KEY;


