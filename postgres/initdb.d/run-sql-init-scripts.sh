#!/bin/sh

#set -e   
set -x
POSTGRES_USER="postgres" 
POSTGRES_DB="sie"
function run_sql_script() {
    psql -v ON_ERROR_STOP=1 -U "$POSTGRES_USER" -d "$POSTGRES_DB" -f "$1"
}

for PSQL in $(find /docker-entrypoint-initdb.d/init -name '*.psql' | sort); do
    run_sql_script $PSQL
done

for PSQL in $(find /docker-entrypoint-initdb.d/init-data -name '*.psql' | sort); do
    run_sql_script $PSQL
done

BACKUPS="$(find /docker-entrypoint-initdb.d/backup -type f \! -name '.*' -print)"
if test -n "$BACKUPS && $INITIALIZE" ; then
    for BACKUP in $(echo "$BACKUPS" | sort); do
        BASENAME=${BACKUP##*/}
        EXT=${BASENAME##*.} 
	if [[ $BACKUP == *"superset"* ]]; then  
	python3 /create-db.py   
	# wait until script is finished
	sleep 10
        psql -U "$POSTGRES_USER" -w -d superset -f "$BACKUP"
        fi
        if test "$EXT" = 'sql' -o "$EXT" = 'psql'; then
            psql -U "$POSTGRES_USER" -w -d "$POSTGRES_DB" -f "$BACKUP"
        fi
    done
fi 
