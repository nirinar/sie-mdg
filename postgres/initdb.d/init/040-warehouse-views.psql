CREATE OR REPLACE VIEW warehouse.reseaux_fr
AS SELECT r.id_reseau,
    r.tension,
    r.longueur,
    r.type,
    r.existant,
    r.the_geom,
    r.import,
    rt.nom AS type_fr
   FROM warehouse.reseaux r
     LEFT JOIN warehouse.reseau_type rt ON r.type = rt.id;


CREATE OR REPLACE VIEW warehouse.matrice_complete
AS SELECT etl_matrice.code,
    etl_matrice.mois,
    etl_matrice.valeur,
    etl_matrice.id_societe,
    etl_matrice.centrale,
    codes_matrice.categorie,
    codes_matrice.renouvelable,
    codes_matrice.unite,
    codes_matrice.tension,
    codes_matrice.consommateur,
    centrales.nom AS nom_de_centrale,
    centrales.district_pcode,
    company.name AS nom_de_societe,
    district.name AS nom_de_district,
    district.region_pcode,
    region.name AS nom_de_region,
    codes_matrice.type_energie,
    date_part('year'::text, etl_matrice.mois)::text AS annee,
    region.taille_menage,
    region.taille_menage_rural,
    region.taille_menage_urbain,
    district_population.population,
    COALESCE(centrales_jirama.milieu, 'R'::bpchar) AS milieu,
    'MG-' || region.pcode as region_iso,
    'MG' || region.pcode || district.pcode as district_iso
   FROM warehouse.etl_matrice
     JOIN warehouse.codes_matrice ON etl_matrice.code::text = codes_matrice.code::text
     JOIN warehouse.centrales ON etl_matrice.centrale::text = centrales.code::text AND etl_matrice.id_societe = centrales.societe
     JOIN warehouse.company ON etl_matrice.id_societe = company.id
     JOIN warehouse.district ON centrales.district_pcode::text = district.pcode::text
     JOIN warehouse.region ON district.region_pcode::text = region.pcode::text
     LEFT JOIN warehouse.district_population ON district_population.district_pcode::text = district.pcode::text AND date_part('year'::text, etl_matrice.mois) = district_population.year::double precision
     LEFT JOIN warehouse.centrales_jirama ON centrales.code::text = centrales_jirama.id::text AND centrales.societe = '11556ea8-3b66-11e9-ae4e-0242113dc113'::uuid; 

CREATE OR REPLACE VIEW warehouse.matrice_complete_copy
AS SELECT etl_matrice_copy.code,
    etl_matrice_copy.mois,
    etl_matrice_copy.valeur,
    etl_matrice_copy.id_societe,
    etl_matrice_copy.centrale,
    codes_matrice.categorie,
    codes_matrice.renouvelable,
    codes_matrice.unite,
    codes_matrice.tension,
    codes_matrice.consommateur,
    centrales.nom AS nom_de_centrale,
    centrales.district_pcode,
    company.name AS nom_de_societe,
    district.name AS nom_de_district,
    district.region_pcode,
    region.name AS nom_de_region,
    codes_matrice.type_energie,
    date_part('year'::text, etl_matrice_copy.mois)::text AS annee,
    region.taille_menage,
    region.taille_menage_rural,
    region.taille_menage_urbain,
    district_population.population,
    COALESCE(centrales_jirama.milieu, 'R'::bpchar) AS milieu,
    'MG-' || region.pcode as region_iso,
    'MG' || region.pcode || district.pcode as district_iso
   FROM warehouse.etl_matrice_copy
     JOIN warehouse.codes_matrice ON etl_matrice_copy.code::text = codes_matrice.code::text
     JOIN warehouse.centrales ON etl_matrice_copy.centrale::text = centrales.code::text AND etl_matrice_copy.id_societe = centrales.societe
     JOIN warehouse.company ON etl_matrice_copy.id_societe = company.id
     JOIN warehouse.district ON centrales.district_pcode::text = district.pcode::text
     JOIN warehouse.region ON district.region_pcode::text = region.pcode::text
     LEFT JOIN warehouse.district_population ON district_population.district_pcode::text = district.pcode::text AND date_part('year'::text, etl_matrice_copy.mois) = district_population.year::double precision
     LEFT JOIN warehouse.centrales_jirama ON centrales.code::text = centrales_jirama.id::text AND centrales.societe = '11556ea8-3b66-11e9-ae4e-0242113dc113'::uuid;


CREATE OR REPLACE VIEW warehouse.region_details
AS SELECT region.pcode,
    region.name,
    ( SELECT sum(dp2.population) AS sum
           FROM warehouse.district_population dp2
             JOIN warehouse.district d2 ON d2.pcode::text = dp2.district_pcode::text AND d2.region_pcode::text = region.pcode::text AND date_part('year'::text, matrice_connexions_p.mois) = dp2.year::double precision) AS population,
    sum(matrice_connexions_p.valeur * region.taille_menage *
        CASE centrales_jirama.milieu
            WHEN 'U'::bpchar THEN 1.7
            ELSE 1.3
        END::double precision) AS desservi,
    sum(matrice_connexions_p.valeur) AS connexions,
    matrice_connexions_p.mois AS date,
    ('MG-'::text || region.pcode::text) AS iso
   FROM warehouse.district
     JOIN warehouse.region ON district.region_pcode::text = region.pcode::text
     LEFT JOIN warehouse.centrales ON centrales.district_pcode::text = district.pcode::text
     LEFT JOIN warehouse.centrales_jirama ON centrales.societe = '11556ea8-3b66-11e9-ae4e-0242113dc113'::uuid AND centrales.code::text = centrales_jirama.id::text
     LEFT JOIN warehouse.etl_matrice matrice_connexions_p ON matrice_connexions_p.centrale::text = centrales.code::text AND matrice_connexions_p.id_societe = centrales.societe AND matrice_connexions_p.code::text = 'CO0202'::text
  GROUP BY region.pcode, region.name, matrice_connexions_p.mois; 


CREATE OR REPLACE VIEW warehouse.region_details_copy
AS SELECT region.pcode,
    region.name,
    ( SELECT sum(dp2.population) AS sum
           FROM warehouse.district_population dp2
             JOIN warehouse.district d2 ON d2.pcode::text = dp2.district_pcode::text AND d2.region_pcode::text = region.pcode::text AND date_part('year'::text, matrice_connexions_p.mois) = dp2.year::double precision) AS population,
    sum(matrice_connexions_p.valeur * region.taille_menage *
        CASE centrales_jirama.milieu
            WHEN 'U'::bpchar THEN 1.7
            ELSE 1.3
        END::double precision) AS desservi,
    sum(matrice_connexions_p.valeur) AS connexions,
    matrice_connexions_p.mois AS date,
    ('MG-'::text || region.pcode::text) AS iso
   FROM warehouse.district
     JOIN warehouse.region ON district.region_pcode::text = region.pcode::text
     LEFT JOIN warehouse.centrales ON centrales.district_pcode::text = district.pcode::text
     LEFT JOIN warehouse.centrales_jirama ON centrales.societe = '11556ea8-3b66-11e9-ae4e-0242113dc113'::uuid AND centrales.code::text = centrales_jirama.id::text
     LEFT JOIN warehouse.etl_matrice_copy matrice_connexions_p ON matrice_connexions_p.centrale::text = centrales.code::text AND matrice_connexions_p.id_societe = centrales.societe AND matrice_connexions_p.code::text = 'CO0202'::text
  GROUP BY region.pcode, region.name, matrice_connexions_p.mois;


CREATE OR REPLACE VIEW warehouse.district_details
AS SELECT district.pcode,
    district.name,
    district.region_pcode,
    region.name AS region_name,
    district_population.year,
    district_population.population,
    sum(NULLIF(matrice_connexions_p.valeur, 0::double precision) * region.taille_menage *
        CASE centrales_jirama.milieu
            WHEN 'U'::bpchar THEN 1.7
            ELSE 1.3
        END::double precision) AS desservi,
    sum(NULLIF(matrice_connexions_p.valeur, 0::double precision)) AS connexions,
    sum(matrice_coupures_bt.valeur) AS coupures_bt,
    (((district_population.year::text || '-'::text) || months.month) || '-01'::text)::date AS date,
    ('MG'::text || district.region_pcode::text) || district.pcode::text AS iso
   FROM warehouse.district
     JOIN warehouse.region ON district.region_pcode::text = region.pcode::text
     JOIN warehouse.district_population ON district.pcode::text = district_population.district_pcode::text
     FULL JOIN ( SELECT months_1.month
           FROM ( VALUES ('1'::text), ('2'::text), ('3'::text), ('4'::text), ('5'::text), ('6'::text), ('7'::text), ('8'::text), ('9'::text), ('10'::text), ('11'::text), ('12'::text)) months_1(month)) months(month) ON true
     LEFT JOIN warehouse.centrales ON centrales.district_pcode::text = district.pcode::text
     LEFT JOIN warehouse.centrales_jirama ON centrales.societe = '11556ea8-3b66-11e9-ae4e-0242113dc113'::uuid AND centrales.code::text = centrales_jirama.id::text
     LEFT JOIN warehouse.etl_matrice matrice_connexions_p ON matrice_connexions_p.centrale::text = centrales.code::text AND matrice_connexions_p.id_societe = centrales.societe AND matrice_connexions_p.code::text = 'CO0202'::text AND matrice_connexions_p.mois = ((((district_population.year::text || '-'::text) || months.month) || '-01'::text)::date)
     LEFT JOIN warehouse.etl_matrice matrice_coupures_bt ON matrice_coupures_bt.centrale::text = centrales.code::text AND matrice_coupures_bt.id_societe = centrales.societe AND matrice_coupures_bt.code::text = 'CUT02'::text AND matrice_coupures_bt.mois = ((((district_population.year::text || '-'::text) || months.month) || '-01'::text)::date)
  GROUP BY district.pcode, district.name, district.region_pcode, region.name, district_population.year, district_population.population, ((((district_population.year::text || '-'::text) || months.month) || '-01'::text)::date);


CREATE OR REPLACE VIEW warehouse.view_bilan_energie
AS SELECT b.annee AS anne,
    b.code_operation AS code_op,
    o.id AS id_op,
    o.description AS description_operation,
    o.sankey AS sankey_operation,
    o2.sankey AS parent_operation,
    o.total AS is_total,
    o.operator AS operator_op,
    o.is_source,
    b.code_energie,
    iea.name AS nom_energie,
    iea.code_convertion AS code_conv_energie,
    iea_conv.name AS nom_conv,
    b.value AS valeur,
    b.value * iea.factor AS ktep_valeur
FROM warehouse.bilan b
LEFT JOIN warehouse.operation o ON b.code_operation::text = o.code::text
LEFT JOIN warehouse.definition_de_iea iea ON b.code_energie::text = iea.code::text
LEFT JOIN warehouse.definition_de_iea iea_conv ON iea.code_convertion::text = iea_conv.code::text
LEFT JOIN warehouse.operation o2 ON (o.id / 100) = o2.id;


CREATE OR REPLACE VIEW warehouse.view_bilan_sankey2
AS SELECT vbe.anne,
        CASE
            WHEN vbe.is_source = 1 THEN vbe.sankey_operation
            WHEN vbe.is_source = 0 THEN vbe.nom_energie
            ELSE NULL::character varying
        END AS source,
        CASE
            WHEN vbe.is_source = 1 THEN vbe.nom_energie
            WHEN vbe.is_source = 0 THEN vbe.sankey_operation
            ELSE NULL::character varying
        END AS target,
    abs(vbe.ktep_valeur) AS valuer_absolut
   FROM warehouse.view_bilan_energie vbe
  WHERE vbe.is_source IS NOT NULL AND vbe.ktep_valeur <> 0::double precision AND vbe.nom_energie::text <> vbe.sankey_operation::text
UNION
 SELECT view_bilan_energie.anne,
    view_bilan_energie.nom_energie AS source,
    'Electricité'::character varying AS target,
    abs(view_bilan_energie.ktep_valeur) AS valuer_absolut
   FROM warehouse.view_bilan_energie
  WHERE view_bilan_energie.code_conv_energie::text = 'ELECTRICITY'::text AND view_bilan_energie.ktep_valeur <> 0::double precision AND view_bilan_energie.is_source = 1
UNION
 SELECT vbe.anne,
    vbe.sankey_operation AS source,
    'Autres sources '::text AS target,
    sum(abs(vbe.ktep_valeur)) AS valuer_absolut
   FROM warehouse.view_bilan_energie vbe
  WHERE (vbe.code_op::text = ANY (ARRAY['MAINELEC'::character varying::text, 'AUTOELEC'::character varying::text])) AND vbe.valeur <> 0::double precision
  GROUP BY vbe.anne, vbe.sankey_operation, 'Electricité'::text
UNION
 SELECT vbe_cons.anne,
    vbe_cons.sankey_operation AS source,
    vbe_cons.parent_operation AS target,
    sum(vbe_cons.ktep_valeur) AS valuer_absolut
   FROM warehouse.view_bilan_energie vbe_cons
  WHERE vbe_cons.parent_operation IS NOT NULL AND vbe_cons.valeur <> 0::double precision AND vbe_cons.parent_operation::text <> 'Production'::text
  GROUP BY vbe_cons.anne, vbe_cons.sankey_operation, vbe_cons.parent_operation;


CREATE OR REPLACE VIEW warehouse.hydrocarb
AS SELECT d.code,
    d.mois,
    d.province AS province_code,
    COALESCE(p."name", 'National'::varchar(100)) AS province,
    d.valeur,
    c.cadre,
    c.code_energie,
    iea."name" AS nom_energie,
    c.code_op,
    c.description,
    c.type_gas,
    c.type_prod,
    c.unite
   FROM warehouse.donnees_hydrocarb d
   JOIN warehouse.codes_hydrocarb c ON d.code::TEXT = c.code::TEXT
   LEFT JOIN warehouse.province p ON d.province = p.code
   LEFT JOIN warehouse.definition_de_iea iea ON c.code_energie = iea.code_omh;
