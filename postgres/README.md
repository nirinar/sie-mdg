TODO: Translate to French 
This repository contains the init scripts which are executed during the postgresql build. The parent image of postresql has an entrypoint that is *not* overriden in this Dockerfile. In this entrypoint the /init and /init-data scripts are executed. 

# Conteneur postgresql

Le conteneur « postgresql » contient un serveur de bases de données PostgreSQL 11. Ce serveur de bases de données continet des bases de données, de l'entrepôt de données énergétiques et diverses bases de données de configuration pour les autres composantes. Pendant l'initialisation du système, le serveur est automatiquement configuré avec deux utilisateurs (postgres et sie) et la structure pour l'entrepôt de données énergétiques est initialisée.

S'il existe des sauvegardes de données dans le sous-répertoire `backup` pendant le premier lancement, leurs données seront injectées à l'entrepôt de données. **Attention** : Il faut faire attention de ne pas inclure des sauvegardes de la structure et des données de certains tableaux fondamentaux qui sont automatiquement remplis pendant l'initialisation. Ceux sont :

* country
* province
* region
* region_names
* district
* district_names
* district_population
* commune
* commune_names
* fokontany
* fokontany_names
* codes_matrice
* company
* centrales
* centrales_jirama
* reseau_type
* definition_de_iea
* operation
* codes_hydrocarb

De l'autre côté, les tables de données qu'on peut transférer sont :

* etl_matrice
* reseaux
* donnees_hydrocarb
* bilan
* imports

Toutes les tables et vues pour les données énergétiques se trouvent dans la base de données « sie » dans le schéma « warehouse ».

## Structure

Pour la plupart des analyses ce n'est pas recommandé d'utiliser les tables de la base de données directement, mais des vues qui ont étés créés à partir d'eux. De cette façon, les données sont enregistrées en forme normalisée mais pour les analyses et leur présentation des lignes complétées et des valeurs plus lisibles sont disponibles. Par exemple, le tableau « etl_matrice » ne contiens qu'une référence à un district par ligne. Pour trouver le nom du district ou la région, il faut le joindre à des autres tableaux (« district », « region »), et peut-être encore à d'autres pour ajouter des informations additionnelles comme la population du district (« district_population »).

Toutes ces relations sont déjà résolues par les vues disponibles par défaut dans la base de données. Ceux sont:

| Vue                | Description                                                                                            | Niveaux de désagrégation |
|--------------------|--------------------------------------------------------------------------------------------------------|---------------------------|
| matrice_complete   | Données du secteur d'électricité sur la production, la puissance, les connexions, etc.                 | mois/ans, district        |
| region_details     | Données démographiques sur les régions, y compris la population et la population desservie précalculée | ans, région               |
| district_details   | Comme « region_details », mais sur le niveau des districts                                             | ans, district             |
| view_bilan_energie | Données du bilan énergétique                                                                           | ans, pays                 |
| view_bilan_sankey2 | Données du bilan énergétique préparées pour la graphique Sankey                                        | ans, pays                 |
| hydrocarb          | Importation, prix, etc. des hydrocarbures selon l'OMH                                                  | any, province             |

Pour des détails sur les relations entre les tables et la construction des vues, veuillez consulter les scriptes « 030-warehouse-tables.psql » et « 040-warehouse-views.psql » dans le repertoire `postgres/initdb.d/init` du référentiel de code du système.

Pour des informations sur la structure des données contenues dans ces vues (et les tables derrière eux), veuillez consulter l'« Aide-mémoire pour l'entrepôt de données SIE ».
