#! /usr/bin/env python3
# warning: this file is a copy of the file in the superset/scripts dir
import os
import psycopg2

def main():
    with connect() as conn: # type: psycopg2.extensions.connection
        conn.autocommit = True
        create_user(conn)
        create_db(conn)


def connect() -> psycopg2.extensions.connection:
    """
    Fait la connexion avec le serveur des bases de données
    """

    try:
        pg_password = os.getenv('POSTGRES_PASSWORD')
        return psycopg2.connect(dbname='sie',  user='postgres', password=pg_password)
    except:
        print('Impossible de se connecter au serveur de bases de données')
        raise


def create_user(conn: psycopg2.extensions.connection):
    """
    Crée l'utilisateur pour SuperSet qui ne peut accèsser que son propre base de données (superset)
    """

    with conn.cursor() as cur: # type: psycopg2.extensions.cursor
        cur.execute("DROP ROLE IF EXISTS superset")
        cur.execute("CREATE USER superset WITH PASSWORD 'listen3Scent3Shady'")


def create_db(conn):
    """
    Crée la base de données pour SuperSet
    """

    with conn.cursor() as cur: # type: psycopg2.extensions.cursor
        cur.execute("CREATE DATABASE superset WITH OWNER = superset")


main()
