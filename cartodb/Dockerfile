FROM ubuntu:18.04 AS FDW_BUILD

RUN apt-get update &&\
    apt-get install -y git make libpq-dev postgresql-server-dev-all gcc unixodbc unixodbc-dev &&\
    cd /usr/local/src &&\
    git clone https://github.com/CartoDB/odbc_fdw.git &&\
    cd odbc_fdw &&\
    make


FROM sverhoeven/cartodb

ARG HOST
ARG LDAP_ADMIN_PASSWORD
ARG SIEADM_PASSWORD
ENV HOST=${HOST:-energie.mg}\
    LDAP_ADMIN_PASSWORD=${LDAP_ADMIN_PASSWORD:-t0pSecret!}\
    SIEADM_PASSWORD=${SIEADM_PASSWORD:-t0pSecret!}

COPY --from=FDW_BUILD /usr/local/src/odbc_fdw/odbc_fdw.so /usr/lib/postgresql/10/lib/odbc_fdw.so
COPY --from=FDW_BUILD /usr/local/src/odbc_fdw/odbc_fdw.control /usr/share/postgresql/10/extension/odbc_fdw.control
COPY --from=FDW_BUILD /usr/local/src/odbc_fdw/*.sql /usr/share/postgresql/10/extension/

RUN wget http://de.archive.ubuntu.com/ubuntu/pool/universe/p/psqlodbc/odbc-postgresql_09.03.0300-1_amd64.deb &&\
    apt-get update &&\
    apt-get install -y ./odbc-postgresql_09.03.0300-1_amd64.deb &&\
    apt-get clean all &&\
    cd /cartodb &&\
    service postgresql start && service redis-server start &&\
    echo dev | bundle exec rake user:deletion:by_username[dev] &&\
    bundle exec rake cartodb:features:add_feature_flag['carto-connectors'] &&\
    bundle exec rake cartodb:features:enable_feature_for_all_users['carto-connectors'] &&\
    RAILS_ENV=development && bundle exec rake cartodb:connectors:create_providers && env -u RAILS_ENV &&\
    echo "insert into connector_configurations(id,created_at,updated_at,enabled,max_rows,connector_provider_id) values(uuid_generate_v1(),now(),now(),true,NULL, (select id from connector_providers where name='postgres' limit 1));" | psql -U postgres -d carto_db_development &&\
    cp /cartodb/script/setup_organization.sh /cartodb/script/setup_meeh.sh &&\
    sed -i 's/"example"/"meeh"/' /cartodb/script/setup_meeh.sh &&\
    sed -i 's/"admin4example"/"meehadmin"/' /cartodb/script/setup_meeh.sh &&\
    sed -i 's/"admin@contoso.com"/"admin@mineau.gov.mg"/' /cartodb/script/setup_meeh.sh &&\
    sed -i "s/\"pass1234\"/\"$SIEADM_PASSWORD\"/" /cartodb/script/setup_meeh.sh &&\
    /bin/bash /cartodb/script/setup_meeh.sh &&\
    bundle exec rake cartodb:features:enable_feature_for_organization['carto-connectors','meeh'] &&\
    bundle exec rake cartodb:connectors:set_user_config["postgres","meehadmin","true",nil] &&\
    bundle exec rake cartodb:connectors:set_org_config["postgres","meeh","true",nil] &&\
    echo "CREATE EXTENSION IF NOT EXISTS odbc_fdw" | psql -U postgres -d $(echo "SELECT database_name FROM users WHERE username='meehadmin'" | psql -U postgres -t -d carto_db_development) &&\
    (DN="dc=$(echo $HOST | sed 's/\./,dc=/g')" ORGANIZATION_NAME=meeh HOST=ldap-server PORT=389 CONNECTION_USER="cn=admin,$DN" CONNECTION_PASSWORD="$LDAP_ADMIN_PASSWORD" USER_ID_FIELD=uid DOMAIN_BASES="ou=People,$DN" USERNAME_FIELD=uid EMAIL_FIELD=mail USER_OBJECT_CLASS=inetOrgPerson GROUP_OBJECT_CLASS=organizationalUnit bundle exec rake cartodb:ldap:create_ldap_configuration)

VOLUME [ "/var/lib/postgresql", "/var/lib/redis"]

