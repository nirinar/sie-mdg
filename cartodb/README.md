# Conteneur cartodb

[Carto](https://carto.com/) est un logiciel pour analyser des données sur des cartes, dans l’espace.

Comme l’installation et configuration de Carto est excessivement complexe, on utilise ici un conteneur préfabriqué ([sverhoeven/cartodb](https://hub.docker.com/r/sverhoeven/cartodb/)) qu’on réconfiguère pour le SIE.

Le conteneur de base a certains limites, dont :

* Les bases de données utilisées par Carto sont fortement intégrées et ne peuvent pas simplement être remplacées par des instances extérieurs
* Il n’est pas possible de configurer le logiciel pour le servir par une connexion HTTPS (cf. ces [deux](https://github.com/sverhoeven/docker-cartodb/issues/67) [bogues](https://github.com/sverhoeven/docker-cartodb/issues/52))
* Le logiciel tourne toujours en mode de développement, sacrifiant ainsi la performance

Le logiciel est intégré avec les service LDAP. Une organisation « SIE » est créée et liée aux comptes d’utilisateurs de ce service.
