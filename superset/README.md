# Conteneur superset

[Superset](http://superset.apache.org/) est utilisé pour créer des graphiques et des tableaux de bord avec des analyses des données dans l'entrepôt des données.

## Modifications

Superset est modifié pour le SIE. Ces modifications sont automatiquement appliquées pendant la création du conteneur. On trouve les modifications dans les fichiers `assets-<version de Superset>.patch` du dossier `superset`. Il s'agit de :

* quelques bogues fixés
* l'addition de deux cartes de Madagascar (par région et par district) pour les graphiques Carte
* des traductions additionnelles en français
* des formats de nombres et de dates prédéfinies additionnels

## Configuration

Superset est configuré par le fichier `superset_config.py` qui se trouve dans le répertoire `config`. Il s'agit d'un fichier dans la langue de programmation « Python » où diverses paramètre pour l'exécution du logiciel sont configurés. Voyez ce fichier et trouvez d'autres paramètres de configuration dans le [répertoire de Superset](https://github.com/apache/incubator-superset/blob/master/superset/config.py).

## Intégration

Superset est intégré avec le service LDAP pour les comptes d'utilisateurs. En plus, PostgreSQL (dans le conteneur « postgres ») est utilisé pour enregistrer des données de processus. Une base de données séparée pour ces données est automatiquement créée pendant le premier lancement de Superset.

## Initialisation

Les permissions pour l'utilisateur « Public » sont automatiquement configurées selon les entrés dans le fichier `config/public_perms.txt`. Cela inclus aussi des droits d'accès aux sources de données principales utilisées dans les tableaux de bord publiques.

On peut mettre des sauvegardes de tableaux de bord exportées d'une autre installation de Superset dans le répertoire `init`. Tous sauvegardes trouvées là seront automatiquement importées et les tableaux de bord correspondantes seront automatiquement disponibles.
