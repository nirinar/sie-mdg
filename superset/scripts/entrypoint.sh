#!/bin/sh
set -x
SCRIPT_DIR=$(dirname "$0")

test_for_db() {
    python3 <<EOF
import psycopg2, sys
try:
    psycopg2.connect("dbname='${1:-superset}' user='postgres' host='postgresql' password='$POSTGRES_PASSWORD'").close()
    exit(0)
except (Exception, psycopg2.DatabaseError):
    exit(42)
EOF

    return
} 
test_for_postgres() {
    python3 <<EOF
import psycopg2, sys
try:
    psycopg2.connect("dbname='${1:-superset}' user='postgres' host='postgresql' password='$POSTGRES_PASSWORD'").close()
    exit(0)
except (Exception, psycopg2.DatabaseError):
    exit(42)
EOF

    return
}


shut_down_superset() {
    for PID in $(ps -Ao pid,comm | grep -o -P '\d+(?=\s+gunicorn)'); do
        kill -SIGTERM $PID
        wait $PID
    done
    kill -SIGTERM $CELERY_PID
}

# Attend la disponibilité de la base de données
while ! test_for_db sie; do sleep 10; done

SETUP=0

# Si la base de données sie n'existe pas, continue avec le setup
if ! test_for_db; then
    SETUP=1
    python3 "$SCRIPT_DIR/create-db.py" 
    superset db upgrade
    flask fab create-admin --username sieadm --firstname Administrateur --lastname SIE --email "info@${LDAP_HOST}" --password remplace_par_ldap
    superset init
    flask fab create-user --username auto --firstname Utilisateur --lastname Automatique --email "noreply@${LDAP_HOST}" --password remplace_par_ldap --role Alpha 
    else  
	    # make sure db is upgraded in case of superset version updates
	    superset db upgrade
fi

# Lance SuperSet
celery worker --app=superset.tasks.celery_app:app --pool=gevent -Ofair &
CELERY_PID=$!
gunicorn --bind  0.0.0.0:80 \
    --workers $((2 * $(getconf _NPROCESSORS_ONLN) + 1)) \
    --timeout 60 \
    --limit-request-line 0 \
    --limit-request-field_size 0 \
    "superset.app:create_app()" &

# Complète le setup
if test $SETUP -eq 1; then
    while ! curl -s -f http://localhost/health; do sleep 5; done

    . "$SCRIPT_DIR/setup-sie.sh"
fi

# Attend jusqu'on reçoit un signal SIGTERM par Docker et puis quitte SuperSet
trap "shut_down_superset" TERM
wait $CELERY_PID
exit 0
