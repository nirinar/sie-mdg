#!/bin/sh

# Jarre pour le cookie de session (nécessaire pour authentification)
COOKIE_JAR=/tmp/cookies
SCRIPT_DIR=$(dirname "$0")

# Produire le prochain jeton de CSRF (écrit à stdout)
get_token() {
    curl -s -b "$COOKIE_JAR" http://localhost/superset/csrf_token/\
        | python3 -c 'import sys, json; print(json.load(sys.stdin)["csrf_token"])'
}

# Perform login and fill session cookie into jar. Logs in as sieadm with that user's initial password since it should
# run during setup. This must always be called first as all other functions rely on the presence of the session cookie.
login() {
    CSRF_TOKEN=$(curl -s -c "$COOKIE_JAR" http://localhost/login/ | grep -o -P '(?<=name="csrf_token" type="hidden" value=")[^"]+(?=")')
    curl -s -b "$COOKIE_JAR" -c "$COOKIE_JAR" -X POST --data-urlencode "csrf_token=$CSRF_TOKEN" -d "username=sieadm" --data-urlencode "password=$SIEADM_PASSWORD" http://localhost/login/ > /dev/null 2>&1
}

# Ajoute une base de données pour l'usage en SuperSet
#
# $1 - Nom de la base de données en PostgreSQL
# [$2] - User visible label of database. Defaults to $1 if omitted.
add_db() {
    CSRF_TOKEN=$(get_token)
    curl -s -X POST -b "$COOKIE_JAR"\
        -F "csrf_token=$CSRF_TOKEN"\
        -F "database_name=${2:-$1}"\
        -F "sqlalchemy_uri=postgresql://sie:sie@postgresql:5432/$1"\
        -F "cache_timeout="\
        -F "expose_in_sqllab=y"\
        -F "force_ctas_schema="\
        -F 'extra={"metadata_params":{},"engine_params":{},"metadata_cache_timeout":{},"schemas_allowed_for_csv_upload":[]}'\
        http://localhost/databaseview/api/create | grep -o -P '(?<=\(id:)\d+(?=\))'
}

# Get the numeric ID of a database by name (écrit à stdout)
#
# $1 - Full name (as in PostgreSQL) of the database
get_db_id() {
    curl -s -b "$COOKIE_JAR" "http://localhost/databaseview/api/read?_flt_1_sqlalchemy_uri=$1"\
        | python3 -c 'import sys, json; print(json.load(sys.stdin)["pks"][0])'
}

# Add a table for use in SuperSet. Prints the numeric ID of the newly added table.
#
# $1 - Full name (as in PostgreSQL) of the database. This database must have been added to SuperSet before (e.g. through
#      add_db).
# $2 - Name of the schema the table resides in
# $3 - Name of the table
add_table() {
    CSRF_TOKEN=$(get_token)
    DB_ID=$(get_db_id $1)
    curl -s -X POST -b "$COOKIE_JAR"\
        -F "csrf_token=$CSRF_TOKEN"\
        -F "database=$DB_ID"\
        -F "schema=$2"\
        -F "table_name=$3"\
        http://localhost/tablemodelview/api/create | grep -o -P '(?<=\(id:)\d+(?=\))'
}

# Get the numeric ID of a table (écrit à stdout)
#
# $1 - Numeric ID of the database
# $2 - Database Schema
# $3 - Full table name
get_table_id() {
    curl -s -b "$COOKIE_JAR" "http://localhost/tablemodelview/api/read?_flt_0_database=$1&_flt_3_schema=$2&_flt_3_table_name=$3"\
        | python3 -c 'import sys, json; print(json.load(sys.stdin)["pks"][0])'
}

# Set description field of table record
#
# $1 - Numeric ID of the database
# $2 - Numeric ID of the table
# $3 - Textual description of the table
set_table_description() {
    CSRF_TOKEN=$(get_token)
    curl -s -X PUT -b "$COOKIE_JAR"\
        --data-urlencode "csrf_token=$CSRF_TOKEN"\
        --data-urlencode "description=$3"\
        -d "database=$1"\
        "http://localhost/tablemodelview/api/update/$2" > /dev/null 2>&1
}

# Change the user visible lable of a table column
#
# $1 - Numeric ID of table
# $2 - New label for column
set_column_label() {
    CSRF_TOKEN=$(get_token)
    COL_PK=$(curl -s -b "$COOKIE_JAR" "http://localhost/tablecolumninlineview/api/read?_flt_0_table=$1&_flt_3_column_name=$2"\
        | python3 -c 'import sys, json; print(json.load(sys.stdin)["pks"][0])')
    curl -s -X PUT -b "$COOKIE_JAR"\
        --data-urlencode "csrf_token=$CSRF_TOKEN"\
        --data-urlencode "verbose_name=$3"\
        "http://localhost/tablecolumninlineview/api/update/$COL_PK" > /dev/null 2>&1
}

import_dashboards() {
    for FILE in $(find init -type f -not -name '.*'); do
        CSRF_TOKEN=$(get_token)
        curl -s -X POST -b "$COOKIE_JAR"\
            -F "csrf_token=$CSRF_TOKEN"\
            -F "file=@$FILE;type=application/json"\
            http://localhost/superset/import_dashboards > /dev/null 2>&1
    done
}

set_public_perms() {
    CSRF_TOKEN=$(get_token)
    curl -s -b "$COOKIE_JAR" http://localhost/roles/api\
        | python3 "$SCRIPT_DIR/map-perms.py" "$SCRIPT_DIR/../local-config/public_perms.txt"\
        | curl -s -b "$COOKIE_JAR" -X PUT -F "csrf_token=$CSRF_TOKEN" -F "name=Public" -K - http://localhost/roles/api/update/2
}

login
DB=$(add_db sie "Entrepôt de données SIE")

TABLE=$(add_table sie warehouse matrice_complete)
set_table_description $DB $TABLE "Matrice de toutes les donnée sur l'électricité"
set_column_label $TABLE code Code
set_column_label $TABLE mois Mois
set_column_label $TABLE valeur Valeur
set_column_label $TABLE id_societe "Code de société"
set_column_label $TABLE centrale "Code de centrale"
set_column_label $TABLE categorie "Catégorie de valeur"
set_column_label $TABLE renouvelable "Energie renouvelable (code)"
set_column_label $TABLE unite "Unité de valeur"
set_column_label $TABLE tension "Moyenne/basse tension"
set_column_label $TABLE consommateur "Type de consommateur (code)"
set_column_label $TABLE nom_de_centrale "Nom de centrale"
set_column_label $TABLE district_pcode "Code de district"
set_column_label $TABLE nom_de_societe "Société"
set_column_label $TABLE nom_de_district "District"
set_column_label $TABLE region_pcode "Code de région"
set_column_label $TABLE nom_de_region "Région"
set_column_label $TABLE renouvelable_fr "Energie renouvelable"
set_column_label $TABLE tension_fr "Tension"
set_column_label $TABLE consommateur_fr "Type de consommateur"
set_column_label $TABLE type_energie "Type d'énergie (code)"
set_column_label $TABLE type_energie_fr "Type d'énergie"
set_column_label $TABLE annee "Année"
set_column_label $TABLE taille_menage "Taille ménage"
set_column_label $TABLE taille_menage_rural "Taille ménage rural"
set_column_label $TABLE taille_menage_urbain "Taille ménage urbain"
set_column_label $TABLE population "Population"
set_column_label $TABLE milieu "Milieu"

TABLE=$(add_table sie warehouse region_details)
set_table_description $DB $TABLE "Détails démographiques sur les régions"
set_column_label $TABLE pcode "Code d'identification"
set_column_label $TABLE name Nom
set_column_label $TABLE population Population
set_column_label $TABLE desservi "Population desservie"
set_column_label $TABLE connexions "Connexions aux reseaux électriques"
set_column_label $TABLE date Date
set_column_label $TABLE iso "Identifiant pour des cartes"

TABLE=$(add_table sie warehouse district_details)
set_table_description $DB $TABLE "Détails démographiques sur les districts"
set_column_label $TABLE pcode "Code d'identification"
set_column_label $TABLE name Nom
set_column_label $TABLE region_pcode "Code d'identification de la région"
set_column_label $TABLE region_name "Nom de la région"
set_column_label $TABLE year "Année (numérique)"
set_column_label $TABLE population Population
set_column_label $TABLE desservi "Population desservie"
set_column_label $TABLE connexions "Connexions aux reseaux électriques"
set_column_label $TABLE coupures_bt "Coupures dans le réseau de basse tension"
set_column_label $TABLE date Date
set_column_label $TABLE iso "Identifiant pour des cartes"

TABLE=$(add_table sie warehouse view_bilan_energie)
set_table_description $DB $TABLE "Données extraitées du bilan énergétique"
set_column_label $TABLE anne "Année (date)"
set_column_label $TABLE year "Année (numérique)"
set_column_label $TABLE code_op "Code d'opération"
set_column_label $TABLE id_op "Identifiant d'opération"
set_column_label $TABLE description_operation "Description de l'opération"
set_column_label $TABLE is_total "Valeur est un total?"
set_column_label $TABLE operator_op "Opérateur de l'opération"
set_column_label $TABLE is_source "Est-ce une source?"
set_column_label $TABLE code_energie "Code IEA du type d'énergie"
set_column_label $TABLE nom_energie "Nom du type d'énergie"
set_column_label $TABLE code_conv_energie "Code de conversion d'énergie"
set_column_label $TABLE valeur "Valeur (unité originale)"
set_column_label $TABLE ktep_valeur "Valeur (ktep)"
set_column_label $TABLE sankey_operation "Opération pour la graphique Sankey"
set_column_label $TABLE nom_conv "Nom de la conversion"
set_column_label $TABLE parent_operation "Opération parente"

TABLE=$(add_table sie warehouse view_bilan_sankey2)
set_table_description $DB $TABLE "Données du bilan énergétique préparées pour la grphique Sankey"
set_column_label $TABLE anne "Année (date)"
set_column_label $TABLE source "Source du flux"
set_column_label $TABLE target "Cible du flux"
set_column_label $TABLE valeur_absolut "Valeur d'énergie en flux"
set_column_label $TABLE year "Année (numérique)"

TABLE=$(add_table sie warehouse hydrocarb)
set_table_description $DB $TABLE "Données hydrocarbures"
set_column_label $TABLE code "Code interne designant le type de données"
set_column_label $TABLE mois "Mois"
set_column_label $TABLE province_code "Code de la province"
set_column_label $TABLE province "Nom de la province"
set_column_label $TABLE valeur Valeur
set_column_label $TABLE cadre "Cadre (local/international/moyen)"
set_column_label $TABLE code_energie "Type d'énergie (code interne)"
set_column_label $TABLE nom_energie "Type d'énergie"
set_column_label $TABLE code_op "Opération"
set_column_label $TABLE description "Description de la valeur"
set_column_label $TABLE type_gas 'Type de gas'
set_column_label $TABLE type_prod 'Type du produit'
set_column_label $TABLE unite 'Unité'

import_dashboards

set_public_perms
