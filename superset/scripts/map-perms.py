#! /usr/bin/env python3

from html.parser import HTMLParser
import json
import sys

roles = {}

class OptionsParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        if tag == 'option':
            for attr in attrs:
                if attr[0] == 'value':
                    self.opt_id = int(attr[1])

    def handle_endtag(self, tag):
        roles[self.opt_name] = self.opt_id

    def handle_data(self, data):
        self.opt_name = data


json_data = json.loads(sys.stdin.read(-1))

html_data = json_data['search_fields']['permissions']
parser = OptionsParser()
parser.feed(html_data)

if len(sys.argv) > 2:
    perm_names = sys.argv[2:]
    separator = ' '
else:
    with open(sys.argv[1]) as perms_file:
        perm_names = {line.strip() for line in perms_file.readlines() if line.strip()[0] != '#'}
    separator = '\n'

print(separator.join({'-F "permissions=%s"' % (roles[name]) for name in perm_names}))
