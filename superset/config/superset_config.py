import ldap
import os
import re

def getEnvSubstitute(name):
    if (name == 'LDAP_BASE_DN'):
        return 'dc=' + ',dc='.join((os.getenv('LDAP_HOST') or 'sie-madagascar.info').split('.'))
    else:
        return ''

def fixenv(name):
    origVal = os.getenv(name)
    newVal = ''
    lastEnd = 0
    for match in re.finditer(r'\{\{(.*?)\}\}', origVal):
        newVal += origVal[lastEnd:match.start()]
        newVal += getEnvSubstitute(match.group(1))
        lastEnd = match.end()
    newVal += origVal[lastEnd:]
    return newVal

# LDAP for user accounts
if ('LDAP_URL' in os.environ) and ('LDAP_SEARCH' in os.environ) and ('LDAP_BIND_USER' in os.environ) and ('LDAP_BIND_PASSWORD' in os.environ):

    from flask_appbuilder.security.manager import AUTH_LDAP

    AUTH_TYPE = AUTH_LDAP

    AUTH_LDAP_SERVER = fixenv('LDAP_URL')
    AUTH_LDAP_SEARCH = fixenv('LDAP_SEARCH')
    if 'LDAP_FILTER' in os.environ and len(os.getenv('LDAP_FILTER')) > 0:
        AUTH_LDAP_SEARCH_FILTER = fixenv('LDAP_FILTER')

    AUTH_LDAP_BIND_USER = fixenv('LDAP_BIND_USER')
    AUTH_LDAP_BIND_PASSWORD = fixenv('LDAP_BIND_PASSWORD')
    AUTH_LDAP_UID_FIELD = "uid"
    AUTH_LDAP_EMAIL_FIELD = "mail"
    AUTH_LDAP_FIRSTNAME_FIELD = "givenName"
    AUTH_LDAP_LASTNAME_FIELD = "sn"
    AUTH_USER_REGISTRATION = True
    AUTH_USER_REGISTRATION_ROLE = "Alpha"

# Enable MapBox maps
MAPBOX_API_KEY = os.getenv('MAPBOX_API_KEY', 'pk.eyJ1Ijoic2llLW1hZGFnYXNjYXIiLCJhIjoiY2p4b3YxbWZlMGE3azNtcm52b3d3OWp6MCJ9.iVblVwmLM1Oau4B_cyY9eQ')

# Network configuration for running behind a reverse proxy and enabling API calls
ENABLE_PROXY_FIX = True
ENABLE_CORS = True
HTTP_HEADERS = {}

# Make it French by default
BABEL_DEFAULT_LOCALE = 'fr'

# Store process data in PostgreSQL
POSTGRES_USER = 'superset'
POSTGRES_PASSWORD = 'listen3Scent3Shady'
POSTGRES_HOST = 'postgresql'
POSTGRES_PORT = 5432
POSTGRES_DB = 'superset'

SQLALCHEMY_DATABASE_URI = 'postgresql://%s:%s@%s:%s/%s' % (POSTGRES_USER,
                                                           POSTGRES_PASSWORD,
                                                           POSTGRES_HOST,
                                                           POSTGRES_PORT,
                                                           POSTGRES_DB)

REDIS_HOST='redis'
REDIS_PORT=6379

# Configure cache using Redis
if (os.getenv('USE_CACHE', '0') == '1'):
    CACHE_CONFIG = {
        'CACHE_TYPE': 'redis',
        'CACHE_DEFAULT_TIMEOUT': 60 * 60 * 12,
        'CACHE_KEY_PREFIX': 'superset_result',
        'CACHE_REDIS_URL': 'redis://%s:%s/2' % (REDIS_HOST, REDIS_PORT)
    }

class CeleryConfig(object):
    BROKER_URL = 'redis://%s:%s/0' % (REDIS_HOST, REDIS_PORT)
    CELERY_IMPORTS = ('superset.sql_lab', )
    CELERY_RESULT_BACKEND = 'redis://%s:%s/1' % (REDIS_HOST, REDIS_PORT)
    CELERY_ANNOTATIONS = {'tasks.add': {'rate_limit': '10/s'}}
    CELERY_TASK_PROTOCOL = 1


CELERY_CONFIG = CeleryConfig

# Flask session cookie options
SESSION_COOKIE_HTTPONLY = False # Solving The CSRF session token is missing when embed superset in iframe, defaut True
SESSION_COOKIE_SAMESITE = None # One of [None, 'Lax', 'Strict']
