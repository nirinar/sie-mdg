# Conteneur Joomla

Joomla est un système de gestion de contenu (SGC/CMS). L'idée derrière l'emploi d'un tel système est de simplifier la création et la gestion d'un site Web même pour des utilisateurs non-techniques.

Pour le SIE, Joomla est employé pour la présentation du site au public, c'est-à-dire, pour héberger le site public.

Le conteneur officiel de Joomla est utilisé comme base, donnant toujours la plus récente version.

## Données

Le conteneur enregistre deux types de données:

1. Le code source des différentes modules et la configuration de base dans un volume de Docker (`joomla-storage` par défaut, lié à `/var/www/html`) et
2. les contenus propres et la configuration du design, etc. dans une base de données MariaDB (le conteneur `mariadb`)

On peut toujours remplacer le conteneur avec une vérsion plus récente et le site va continuer à marcher, si on ne touche pas ces deux référentielles de données.

## Configuration

On a ajouté le module [Akeeba Backup](https://www.akeebabackup.com/) pour faciliter la migration du site Joomla. Une migration se déroule en trois étapes :

1. Création d'une sauvegarde sur le site existant
2. Copier la sauvegarde dans le nouveau conteneur
3. Lancement du processus de restauration

### Création d'une sauvegarde

Une sauvegarde est créée par l'interface utilisateur de Akeeba Backup. On le trouve dans la section administrative de Joomla dans le menu « Composants » → « Akeeba Backup ». Sur l'écran de Akeeba Backup on trouve dans la section « Basic Operation » les deux buttons « Backup Now » et « Manage Backups ».

« Backup Now » va créer une nouvelle sauvegarde après un clic. Après ce processus a terminé, on peut télécharger cette sauvegarde. Pour la télécharger, on appuie sur le bouton « Manage Backups » et, sur l'écran suivant, sur le bouton « Download » pour le sauvegarde qu'on veut utiliser pour la migration (normalement le plus récent).

### Copier la sauvegarde dans le nouveau conteneur

Ils existent deux possibilités pour copier la sauvegarde dans un conteneur.

La première possibilité est de copier le fichier dans le répertoire `joomla/backup` du répertoire de l'ensemble des conteneurs Docker (le sous-repertoire `backup` rélatif au lieu original de cette documentation). Après, on peut lancer la construction du conteneur et le fichier de sauvegarde sera automatiquement intégré. Il faut bien faire attention que le conteneur **soit vraiment construit de nouveau**, par exemple supprimant l'image précédent si ça existe ou en ajoutant le paramètre `--build` à la ligne de commande de `docker-compose`, et de **supprimer le volume précédent** si ça existe. Sinon, le fichier de sauvegarde ne sera pas disponible dans la prochaine étape !

La deuxième possibilité est simplement de copier le fichier de sauvegarde dans la racine du volume `joomla-storage` (trouvé au chemin indiqué dans la [section sur les volumes](#volumes), si le volume a déjà été créé). Il faut bien considérer qu'avec cette méthode il est bien possible qu'il reste des résidus d'une installation précédente de Joomla qui peuvent changer le site ou rendre l'importation et la sauvegarde impossible dans la prochaine étape.

### Le processus de restauration

Après que le fichier ai été copié et que le conteneur tourne, on visite la page de « Akeeba Kickstarter », par ex. à http://www.sie-madagascar.info/kickstart.php (il faut modifier ici le nom du serveur et changer la partie `www.sie-madagascar.info` avec l'adresse correcte). Kickstarter va vous diriger sur plusieurs écrans pour la configuration de la restauration.

1. La première chose est un écran informatif que vous pouvez simplement fermer par appuyant sur le bouton « Click here or press ESC to close this message ».
2. Sur l'écran derrière la box informative il faut choisir le fichier dans le champ **« Archive File »** (normalement on n'aura qu'un seul fichier disponible). En plus, il faut changer le champ **« Write to Files » en « Directly »**. Les autres champs ne sont pas changés. On appuie sur « Start ». Kickstarter va maintenant extraire le contenu du fichier de sauvegarde. Après la finalisation de l'extraction on appuie sur « Run the Installer » (lancer l'installation).
3. Le logiciel d'installation vous donne d'abord un écran de revue de la configuration. On continue en cliquant sur le bouton « Next → » (suivant) au droit en haute.
4. Dans les écrans qui suivent, on doit configurer quelques paramètres du site, y compris :
   1. Les paramètres de la base de données. Le « Database type » n'est pas changé. Le hôte (« Database server host name ») est `mariadb`, le nom d'utilisateur (« User name ») est `root`, le mot de passe (« Password ») est celui que vous avez configurées pour la base de données dans le fichier `.env` et le nom de la base de données (« Database name ») est `joomla`. Notez le préfixe comme ce sera nécessaire après. Les autres paramètres ne sont pas changés.
   2. Le lien principal du site (« Live site URL »). Il faut le modifier et le mettre en accord avec le nom de domaine du serveur utilisé.
   3. Quelques autres paramètres comme le nom du site et le compte d'administrateur.
5. Quand on arrive à la fin du processus, on peut fermer la fenêtre pour retourner à la première fenêtre de Kickstarter.
6. Là, on appuie sur « Clean Up » pour finaliser l'installation.

Après l'installation est finie on peut accéder au site régulièrement.

## Etapes après l'installation

Après l'installation, on doit encore ajuster les liens aux tableaux de bord. Cela est nécessaire parce que ces liens se trouvent dans des données statiques qui forment la base du site. Normalement on veut que ces liens pointent vers les composantes Superset et Carto de la même installation, et alors il faut les corriger pour le nouveau nom de domaine. Il-y-a dans le logiciel « Shell » (le site interne) une fonctionnalité pour faire ces modifications semi-automatiquement.

Cette fonctionnalité est disponible pour les administrateurs de données (data_admins) exclusivement. On la trouve dans le menu (à droit, en haute) du site interne sous le point « Corriger liens en Joomla ». Sur cet écran de correction, on doit d'abord choisir le préfixe qu'on a noté dans l'étape précédente (normalement on ne trouve qu'un seul préfixe dans la liste). Après, il faut choisir les domaines qu'on veut corriger de la liste de tous les domaines que le système a pu trouver dans la base de données et entrer le nouveau domaine.

Après avoir cliqué sur « Effectuer » les changements seront automatiquement faits.

*Attention* : Le système ne vérifie pas les liens résultants. La vérification est dons quelque chose qui doit être fait par l'administrateur. En plus, même après cette migration, les liens aux cartes de Carto ne seront pas normalement correctes, comme ces liens sont toujours régénérés et sont alors différentes pour chaque installation de Carto. Il est nécessaire de corriger ces liens manuellement dans les modules respectifs de Joomla.
