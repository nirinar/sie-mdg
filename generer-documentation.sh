#!/bin/sh

if [ -z $(which pandoc) ]; then
    echo "Impossible de trouver le logiciel pandoc. Veuillez l'installer pour la génération de la documentation."
    exit 1
fi

READMES="./README-titre.md ./README.md $(find . -name README.md -not -path ./README.md -print)"

pandoc -t docx -o documentation.docx --toc $READMES
