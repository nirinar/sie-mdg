# Conteneur shell-backend

Ce conteneur contient le logiciel avec l'API du SIE. L'API expose des fonctionnalitées de gestion pour le système. L'utilisateur n'interagit jamais directement avec le back end mais avec le front end qui se trouve dans le conteneur « shell ». Le front end va lancer des requêtes envers le back end selon les actions de l'utilisateur.

## Composantes

Le back end est composé à partir de deux repos de git. L'un est le repos du logiciel backend propre. Il s'agit d'un logiciel écrit en TypeScript et automatiquement transformé en JavaScript pour l'exécution sur le platteforme NodeJS.

L'autre repos contient tous les processus d'importation de données prédéfinis pour le SIE. Ces processus sont préparés pour un lancement automatique par le back end quand une requête d'importation arrive.

## Volume

Le conteneur expose un seul volume, lequel contient des métadonnées sur les tableaux de bord. Ces métadonnées consistent en des catégorisations des tableaux de bord et des commentaires ou notes que les utilisateurs ont ajoutés à eux pour la discussion interne.

En plus, le sous-répertoire `importers` du volume contient tous les processus d'importation disponibles pour la transformation et importation automatique de données dans l'entrepôt de données du SIE.

### Processus d'importation

Pour chacun des processus d'importation il-y-a un sous-répertoire dans le répertoire `importers`. Ce sous-répertoire contient un logiciel pour le chargement des données de source, leur transformation et enregistrement dans l'entrepôt de données. A côté de ce logiciel chaque sous-répertoire doit aussi contenir un fichier `manifest.json` avec la structure suivante :

```json
{
  "name": "nom_de_l_importateur",
  "label": "Nom de l'importateur",
  "description": "Une description de l'importateur et des données de source qu'il peut traiter",
  "example": "fichier_example.xlsx",
  "fileTypes": ".xlsx,.xls",
  "command": "node . '${file}'",
  "output": "sql"
}
```

Les champs dans cette structure sont :

#### name

Le nom interne de l'importateur. Doit être identique au nom du répertoire.

#### label

Le nom de l'importateur comme il sera présenté à l'utilisateur dans l'interface d'utilisateur.

#### description

Une description de l'importateur et des données de source qu'il peut traiter. Cette description sera présentée à l'utilisateur après la sélection d'un importateur et doit lui donner des informations sur les données de source attendues par l'importateur.

#### example

Le nom d'un fichier d'exemple que l'utilisateur peut télécharger comme documentation de la structure du fichier attendue par le logiciel d'importation. Ce fichier doit se trouver à côté du fichier `manifest.json`. *Ce champ est facultatif*.

#### fileTypes

Une liste de types de fichier que le logiciel d'importation est capable de traiter. Les différents types de fichier sont identifiés par leur extension (avec point) et séparés par virgule.

#### command

Une commande qui sera lancée dans un shell pour effectuer l'importation. La commande peut contenir des textes de substitution délimitées par `${` et `}`. Les mots clés disponibles sont :

* `file` : le chemin au fichier téléversé avec les données de source
* `dbHost` : l'adresse du serveur de la base de données (nom de hôte exclusivement)
* `dbPort` : le port de réseau de la base de données sur le serveur
* `dbName` : le nom de la base de données dans laquelle les données doivent être enregistrées
* `dbUser` : le nom d'utilisateur à utiliser pour se connecter à la base de données
* `dbPass` : le mot de passe à utiliser pour se connecter à la base de données

La commande sera lancée dans le répertoire du fichier `manifest.json`.

#### output

Doit être « sql » ou « text ». « sql » indique que le logiciel va générer un scripte SQL à exécuter dans l'entrepôt de données. Le script doit donc être prêt pour PostgreSQL. Le script sera exécuté dans une transaction de base de données.

Tout autre valeur indique que le logiciel enregistrera lui-même les données dans la base de données.

# Processus de transformation de données

À cause de la structure des différents fichiers Excel que le système doit être capable de traiter, ils existent deux différentes types de processus de transformation de données. Ceux sont : 1) Des processus réalisés en scripte (dans la langue de programmation JavaScript) et 2) des processus réalisés avec le logiciel « Talend DI » qui permet la définition de tels processus dans un mode plutôt graphique.

Quand même que la complexité des processus graphiques de transformation de données et de leur création peut être très considérable, ces processus sont parfois considérés plus accessible que les processus en code, surtout pour des personnes qui ne sont pas familiarisés avec la programmation. Il fallait tout de même implémenter certains processus par code à cause de la structure des données de source. En particulier, les logiciels graphiques de transformation de données comme « Talend DI » ne sont pas prêts pour lire des fichiers à largeur indéfinie comme, par ex., les tableaux d'évolution de la JIRAMA, qui reçoivent toujours de colonnes additionnelles pour chaque nouvelle période (ans ou mois).

Tous les processus de transformation de donnée sont contenus dans le répertoire de code « importateurs ».

## Les processus en scripte

Les processus en scripte se trouvent dans le référentiel de code des importateurs pour le système dans le dossier « packages ». Ils sont tous structurés de la même façon, séparant la logique et la définition de la structure des données de sources et de la structure cible. Comme ça, même si vous n'êtes pas un programmateur et vous ne comprenez pas les code de programmation, vous pouvez toujours rajuster la transformation si, par ex., il-y-a des nouvelles lignes dans une nouvelle version d'un fichier.

Si vous êtes un programmateur, vous pouvez commencer l'exploration du code à partir du fichier `src/index.js` dans chaque dossier d'un processus. Les processus sont destinés à l'exploitation par NodeJS 10.

Les définitions des structures de données se trouvent dans le dossier `src/definitions` pour chaque processus. Là, vous trouvez toujours les deux fichiers `file-structure.js` et `normalization.js`, correspondant respectivement à la lecture du fichier source et à l'écriture dans les structures cibles. Les définitions dans ces fichiers sont en forme d'objet JavaScript, avec chaque définition de ligne de données délimitée par accolades (`{` et `}`) et sépparée par comma et contennant des champs d'informations de configuration, séparés par comma, de la structure `nom: 'valeur'` ou `nom: 42` pour des valeurs numériques. Par example:

```javascript
const FILE_STRUCTURE = [
    { row: 9, property: 'month' },
    { row: 10, property: 'grossProduction' },
    { row: 11, property: 'hydro' },
    { row: 15, property: 'thermalGO' },
    { row: 19, property: 'thermalFO' },
    { row: 23, property: 'thermalSolar' },
    …
];
```

Cette configuration prise du ficher `file-structure.js` indique, par example, que dans le fichier de source Excel le logiciel peut trouver dans la ligne 9 le valeur pour le mois, dans la ligne 10 le valeur pour la production totale etc.

Dans le fichier `normalization.js`, les structures sont similaires:

```javascript
const NORMALIZATIONS = [
    { code: 'EL01', field: 'grossProduction' },
    { code: 'EL015HY', field: 'hydro' },
    { code: 'EL015CG', field: 'thermalGO' },
    { code: 'DL08811', field: 'consumptionGO', modification: fac(1000) },
    { code: 'RF08811', field: 'consumptionFO', modification: fac(1000) },
    { code: 'EC13321', field: 'piHydro' },
    { code: 'EC13341EG', field: 'piThermalGO' },
    { code: 'EL121M', field: 'saleMTPrivate', conditional: nonNet() },
    { code: 'EL1235M', field: 'saleMTAdministration', conditional: nonNet() },
    { code: 'CO01', field: ['connectionsMTPrivate', 'connectionsMTAdministration', 'connectionsMTPumps'], modification: def(0), conditional: nonNet() },
    …
];
```

Ici, `code` est un code pour la table `etl_matrice` dans la base de données. `field` est une référence à un champ lu du fichier de source (ce qui était désigné par `property` au-dessus). Si `field` est une liste de champs, leurs valeurs seront sommées.

Par l'optionnel `modification` vous pouvez ajouter une transformation des données, comme, par exemple, `fac(1000)`, pour une multiplication avec le facteur 1000 pour arriver aux MWh à partir des GWh. Voyez le fichier `normalization-modifications.js` à côté du fichier `normalization.js` pour des descriptions de toutes les modifications disponibles.

Par l'optionnel `conditional` vous pouvez définir des conditions pour l'intégration des données dans la base de données. Dans notre exemple, `nonNet()` est utilisé pour exclure certaines valeurs si elles sont associés avec un réseau interconnecté. Voyez le fichier `normalization-conditions.js` à côté du fichier `normalization.js` pour des descriptions de toutes les conditions disponibles.

*Attention* : Par ces fichiers de configuration il est possible de reconfigurer les logiciels pour l'inclusion de nouvelles lignes de données au futur. Il faut faire attention tout-de-même de ne pas changer la structure exacte des colonnes comme c'est pour ça que ces processus ont été créés en forme de scripte.

## Les processus graphiques

Tous les autres processus de transformation de données sont réalisés par un outil graphique ([Talend DI](https://www.talend.com/products/integrate-your-data/), à télécharger par [Sourceforge](https://sourceforge.net/projects/talend-studio/files/Talend%20Open%20Studio/)). Ils se trouvent dans le répertoire `talend`. Pour leur modification il faut d'abord les importer en « Talend DI ». Pour l'usage de « Talend DI » voyez le manuel utilisateur.

Finalement, tous les processus doivent être « construits », c'est-à-dire, convertis en forme exécutable. Comme, avec la version Open Source de « Talend DI », il n'est pas possible de construire ces processus automatiquement, vous devez les construire manuellement et les mettre dans le répertoire `talend/bin`.

## Effectuer les changements

L'intégration de changements dans des processus de transformation de données nécessite leur téléversement dans le référentiel de code utilisé par le système dans le processus de sa construction. En principe, après ayant fait les modifications et construit des processus de « Talend DI » si nécessaire, ce téléversement se fait par le lancement des commandes suivantes dans le répertoire principal du référentiel des processus:

```sh
git add .
git commit
git push upstream master
```

Après, les processus de transformation de données doivent être réintégrés dans le système pas les commandes suivantes exécutées sur le serveur hôte:

```sh
docker stop sie_shell-backend_1
docker rm sie_shell-backend_1
docker-compose up -d
```
