#!/bin/sh

if [ ! -e /var/lib/meeh-shell/data.ldb ]; then
    mkdir -p /var/lib/meeh-shell/data.ldb
fi

cp -r /tmp/importers/dist /var/lib/meeh-shell/importers

trap 'kill - TERM; kill -TERM -- $$' TERM

/usr/local/bin/node . -p 80 & wait
