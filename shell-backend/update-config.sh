DN="dc=$(echo $HOST | sed 's/\./,dc=/g')"

if test ${HTTPS:-0} -eq 1; then
    PROTOCOL=https
else
    PROTOCOL=http
fi

sed -i "s|{{host-dn}}|$DN|g" src/config/base-config-defaults.ts
sed -i "s|{{ldap-admin-password}}|$LDAP_ADMIN_PASSWORD|g" src/config/base-config-defaults.ts
sed -i "s|{{root-host}}|$HOST|g" src/config/base-config-defaults.ts
sed -i "s|{{sieadm-pw}}|$SIEADM_PASSWORD|g" src/config/base-config-defaults.ts
sed -i "s|{{auto-pw}}|$AUTO_PASSWORD|g" src/config/base-config-defaults.ts
sed -i "s|{{postgres-pw}}|$POSTGRES_PASSWORD|g" src/config/base-config-defaults.ts
sed -i "s|{{protocol}}|$PROTOCOL|g" src/config/base-config-defaults.ts
