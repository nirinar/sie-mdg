#!/bin/sh

if [ "${HTTPS:-0}" -ne 1 ]; then
    exit 0
fi

CERT_CHECK_FILE=/etc/letsencrypt/cert-check-time
echo "Health check on SSL config…"

if [ -e $CERT_CHECK_FILE ]; then
    THRESHOLD=$(date -d 'now - 23 hours' +%s)
    LASTCHECK=$(date -r $CERT_CHECK_FILE +%s)
    if [ $THRESHOLD -lt $LASTCHECK ]; then
        exit 0
    fi
fi

echo "Check threshold passed. Trying certificate creation/update…"

if [ -e /etc/letsencrypt/live/sie/cert.pem ]; then
    echo "Renewing certificate…"
    certbot renew
else
    echo "Requesting new certificates…"
    DOMAINS=""
    for CONF in /etc/nginx/conf.d/*.conf; do
        CONF_DOMAINS=$(grep -oP '(?<=server_name ).+?(?=;)' $CONF)
        for DOMAIN in $(echo $CONF_DOMAINS); do
            if [ -z "$DOMAINS" -o -n "${DOMAINS##*-d $DOMAIN*}" ]; then
                if [ "$DOMAIN" != "_" ]; then
                    DOMAINS="$DOMAINS -d $DOMAIN"
                fi
            fi
        done
    done

    rm -rf /etc/letsencrypt/live
    certbot certonly -n --webroot -w /usr/share/nginx/html $DOMAINS --cert-name sie --agree-tos -m $ADMIN_EMAIL
fi

echo "Reloading NGINX config…"
nginx -s reload
touch $CERT_CHECK_FILE
