#!/bin/sh

# Configuration files for the sites that should be protected with TLS
SSL_SITES="backend-proxy.conf gitea.conf superset.conf"

replace_patterns() {
    sed -i "s/{{hostname}}/$HOST/g" $1
    sed -i "s/{{backend-proto}}/$BACKEND_PROTO/g" $1
    sed -i "s/{{port}}/$NGINX_PORT/g" $1
}

add_cert_conf() {
    echo "Adding certificate data to $1"
    sed -i '/^\s*server_name\s/a \    ssl_certificate /etc/letsencrypt/live/sie/fullchain.pem;' $1
    sed -i '/^\s*ssl_certificate\s/a \    ssl_certificate_key /etc/letsencrypt/live/sie/privkey.pem;' $1
}

# Builds a configuration file that redirects to the HTTPS protected version automatically
build_no_ssl_conf() {
    SERVERNAME=$(grep -oP '(?<=server_name ).+?(?=;)' $1)
    NO_SSL_FN="/build/nginx-conf/$(basename $1 .conf)-no-ssl.conf"
    NO_SSL_CONF="server {
    listen 80;

    server_name $SERVERNAME;

    location /.well-known/ {
        root /usr/share/nginx/html;
    }

    location / {
        return 301 https://\$host\$request_uri;
    }
}"

    echo "Adding no-SSL config $NO_SSL_FN"
    echo "$NO_SSL_CONF" > "$NO_SSL_FN"
}

add_letsencrypt_location() {
    sed -i '/^\}/i \\n    location /.well-known/ {\n        root /usr/share/nginx/html;\n    }' "$1"
}

build_ssl_conf() {
    SERVERNAME=$(grep -oP '(?<=server_name ).+?(?=;)' "$1")
    SSL_FN="/build/nginx-conf/$(basename $1 .conf)-plus-ssl.conf"
    SSL_CONF="server {
    listen 443 ssl;

    server_name $SERVERNAME;

    location / {
        return 301 http://\$host\$request_uri;
    }
}"

    echo "Adding redirecting SSL config $SSL_FN"
    echo "$SSL_CONF" > "$SSL_FN"
}

build_self_signed_certs() {
    DOMAINS=""
    COUNT=0
    for FN in /build/nginx-conf/*.conf; do
        COUNT=$(( COUNT+1 ))
        CONF_DOMAINS=$(grep -oP '(?<=server_name ).+?(?=;)' $FN)
        for DOMAIN in $CONF_DOMAINS; do
            if [ "$DOMAIN" != "_" ]; then
                DOMAINS="${DOMAINS}DNS.$COUNT = $DOMAIN\n"
            fi
        done
    done

    mkdir -p /build/cert/sie
    openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /build/cert/sie/privkey.pem -out /build/cert/sie/fullchain.pem -config - << EOF
[req]
distinguished_name = req_distinguished_name
req_extensions = v3_req
prompt = no
[req_distinguished_name]
C = MG
ST = AM
L = Antananarivo
O = MEEH
OU = SIE
CN = www.energie.mg
[v3_req]
keyUsage = keyEncipherment, dataEncipherment
extendedKeyUsage = serverAuth
subjectAltName = @alt_names
[alt_names]
$DOMAINS
EOF
}

if [ ${HTTPS:-0} -eq 1 ]; then
    BACKEND_PROTO=https
    NGINX_PORT="443 ssl http2"
    echo "Preparing HTTPS configuration…"
else
    BACKEND_PROTO=http
    NGINX_PORT=80
fi

for FN in /build/nginx-conf/* /build/shell/src/config/main.json; do
    replace_patterns $FN
done

if [ ${HTTPS:-0} -eq 1 ]; then
    # Create a self-signed certificate so that NGINX can start up for the first time with HTTPS. The first certificate check will then replace the self-signed certificate with a real one.
    build_self_signed_certs

    # Keep track of sites that are not SSLed
    HTTP_SITES=$(basename -a /build/nginx-conf/*.conf)
    for CONF in $(echo $SSL_SITES); do
        add_cert_conf /build/nginx-conf/$CONF
        build_no_ssl_conf /build/nginx-conf/$CONF
        # Remov efrom list of non-SSL sites
        HTTP_SITES="${HTTP_SITES%$CONF*} ${HTTP_SITES#*$CONF}"
    done

    # `echo` here suppresses empty entries that might result from above removal logic
    for CONF in $(echo $HTTP_SITES); do
        add_letsencrypt_location /build/nginx-conf/$CONF
        build_ssl_conf /build/nginx-conf/$CONF
    done
fi
