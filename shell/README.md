# Conteneur shell

NGINX agit comme un reverse proxy pour rediriger les requêtes des utilisateurs envers les différents services qui composent le SIE. Il a aussi la responsabilité pour la cryptographie et la sécurisation des connexions. Ce conteneur contient le logiciel NGINX.

En plus, le conteneur contiens les parties statiques du portail interne pour la gestion du système, c'est-à-dire le Web-App « shell » (d'où le nom du conteneur).

## Configuration des sites

Les fichiers de configuration des différents sites qui font parties du système se trouvent dans le répertoire `nginx`. Ces fichiers seront chargés par NGINX. Chaque fichier contient une section `server` et la configuration pour servir le site correspondant (normalement comme proxy). Les fichiers de configuration peuvent contenir des morceaux délimités par `{{` et `}}` qui seront remplacés par des paramètres de configuration du système. Ces morceaux sont :

* `hostname` : le nom de hôte principal du système comme « sie-madagascar.info »
* `port` : Exclusivement pour les conteneurs qui peuvent être servis par HTTPS. Ce morceau sera remplacé par `80` pour une configuration sans TLS et par `443 ssl` pour une configuration avec TLS.

## SSL/TLS

Si le paramètre `HTTPS` est configuré à `1` dans le fichier de configuration `.env` dans le répertoire principal, le conteneur demandera automatiquement un certificat par LetsEncrypt. Le certificat sera configuré pour les tous les site indiqués dans la variable `SSL_SITES` dans la tête du fichier `update-config.sh`. Pour ces sites, HTTP/2 est automatiquement activé pour des chargements accélérés (en moyen, cela garantie une amélioration d'environ 30 %).

Pour les sites configurés pour HTTPS tout trafic sera automatiquement et forcement redirigé à HTTPS si le site est accédé par HTTP.

**Attention** : Carto ne marche pas avec SSL/TLS dans la présente configuration. Comme les navigateurs refusent de charger un site HTTP*S* ayant des références à des ressources HTTP (sans « S », alors non-sécurisées) pour des raisons de sécurité, il n'est aussi pas possible de servir par HTTP*S* des sites qui encastrent des cartes de Carto. Cela appartient aux deux sites configurés dans les fichiers de configuration `joomla.conf` et `shell.conf`. Pour ces sites (cartodb, joomla, shell), un certificat est installé et une redirection configurée de HTTP*S* à HTTP pour éviter une situation où le site n'est pas chargé après une navigation fautive au site HTTPS.

Le conteneur prend responsabilité de renouveler le certificat avant son expiration.
