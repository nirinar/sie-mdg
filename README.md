# Installation

## Structure du système

Le système est composé de plusieurs conteneurs Docker qui sont automatiquement lancé par le logiciel « docker-compose ». Chaque conteneur agit similairement à une machine virtuelle. A la base d'un conteneur, on trouve un image, contenant les logiciels. Comme les conteneurs agissent comme des machines virtuelles isolés, ils sont connectés à deux réseaux virtuels pour permettre une communication mutuelle. Les données sont enregistrées dans des volumes.

Pour le développement du système il faut que vous vous familiarisiez avec [Docker](https://docs.docker.com/) et [docker-compose](https://docs.docker.com/compose/) et en plus avec les composantes installées dans les conteneurs respectifs.

*Pour simplement lancer et utiliser le système ceci n'est pas nécessaire !* Voyez les sections *« [Configuration](#configuration) »* et *« [Lancer le service](#lancer-le-service) »* si-dessus.

### Réseaux

Le système a deux réseaux pour assurer l'isolation entre les services dirigés à l'externe et ceux qui servent pour les besoins internes. Certains services sont connectés aux deux réseaux comme ils doivent servir des requêtes venant de l'externe et pour y arriver doivent recourir aux services internes.

Les réseaux et les services connectés à eux sont :

| Reseau  | Services                                                                                 |
|---------|------------------------------------------------------------------------------------------|
| private | cartodb, gitea, joomla, ldap-server, mariadb, postgresql, redis, shell-backend, superset |
| public  | cartodb, gitea, joomla, shell, shell-backend, superset                                   |

### Conteneurs

Le système est composé d'un large nombre de conteneurs, un conteneur spécifique pour chaque composante. Les conteneurs sont :

| Conteneur     | Responsabilité                                                                                                                             |
|---------------|--------------------------------------------------------------------------------------------------------------------------------------------|
| cartodb       | Logiciel pour la création et la présentation de données encartes.                                                                          |
| gitea         | Logiciel pour tenir les référentiels de code de source du système.                                                                         |
| joomla        | Site public.                                                                                                                               |
| ldap-server   | Agit comme base de données pour les comptes d'utilisateurs. Nécessaire pour partager les comptes d'utilisateurs avec les autres logiciels. |
| mariadb       | Base de données derrière le site public (Joomla)                                                                                           |
| postgres      | Base de données centrale. Contient l'entrepôt de données et la base de données de configuration de SuperSet.                               |
| redis         | Un cache pour l'accélération de SuperSet.                                                                                                  |
| shell         | Contient le reverse proxy (nginx) et l'interface utilisateurs du ministère.                                                                |
| shell-backend | Contiens la logique derrière le site du MEEH; s'occupe de la gestion du système.                                                           |
| superset      | Logiciel pour la création et la présentation de graphiques et de tableaux de bord.                                                         |

Voyez les fichiers `README.md` dans les dossiers des conteneurs respectifs pour des détails sur le mode de travail et la configuration individuelle.

### Volumes

Les données, soit les données de l'entrepôt de données ou les données de configuration, sont enregistrées dans différents volumes de docker, selon les conteneurs.

Les volumes suivants existent :

| Volume              | Données enregistrées                                   |
|---------------------|--------------------------------------------------------|
| carto-redis-storage | Données additionnelles de gestion de Carto             |
| carto-storage       | Configuration de cartes et base de données de Carto    |
| cert-storage        | Certificat SSL pour les connections sécurisées         |
| gitea-data          | Référentiels de code de source                         |
| joomla-storage      | Modules additionnelles et fichiers statiques de Joomla |
| ldap-lib            | Comptes d'utilisateurs                                 |
| ldap-conf           | Configuration du serveur LDAP                          |
| mariadb-data        | Données et configuration pour JOOMLA/le site public    |
| postgresql-data     | Entrepôt de données et autres bases de données         |
| shell-backend       | Informations de gestion sur les tableaux de bord       |
| superset-home       | Configuration de base de SuperSet                      |

On peut trouver ces volumes sur le disque dur du serveur au chemin `/var/lib/docker/volume/<préfix>_<nom du volume>/_data` pour faire par exemple des sauvegardes. *Attention : La modification des fichiers contenus dans ces volumes peut détruire le système !*

## Configuration

Les sources nécessaires se trouvent dans un dépôt git à l'adresse [https://gitea.energie.mg/sie/shell_docker.git](https://gitea.energie.mg/sie/shell_docker.git). On commence le processus d’installation par copier ce dépôt avec le commande `git clone https://gitea.energie.mg/sie/shell_docker.git`. Les sources se trouvent ensuite dans le répertoire `shell_docker`. On fera référence à ce répertoire dans la suite par l'expression « répertoire racine ».

Les paramètres principaux du SIE sont configurés dans le fichier `.env`. Les paramètres suivants sont disponibles :

| Paramètre             | Valeur par défaut           | Notes                                                                                                                                            |
|-----------------------|-----------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------|
| `HOST`                | `sie-madagascar.info`                    | Doit être adapté au nom du domaine utilisé                                                                                                       |
| `LDAP_ADMIN_PASSWORD` | `t0pSecret!`                | Mot de passe pour le rôle LDAP à `cn=admin,dc=sie-madagascar-madagascar,dc=info` ou pareille (selon le paramètre `HOST`). Normalement utilisé internement.               |
| `POSTGRES_PASSWORD`   | `t0pSecret!`                | Mot de passe pour le rôle `postgres` dans le serveur de bases de données PostgreSQL                                                              |
| `SIEADM_PASSWORD`     | `t0pSecret!`                | Mot de passe pour l'administrateur principal                                                                                                     |
| `AUTO_PASSWORD`       | `t0pSecret!`                | Mot de passe pour l'utilisateur automatique responsable pour des tâches internes                                                                 |
| `HTTPS`               | `0`                         | Mettre `1` ici pour délivrer les pages par HTTPS/TLS. *Ne jamais mettre `1` ici pour un système de développement !*                               |
| `ADMIN_EMAIL`         | `hostmaster@sie-madagascar.info`         | Courriel de l'administrateur, lié au compte `sieadm`                                                                                             |
| `GIT_SOURCE`          | `https://gitea.gfa-demo.de` | Adresse du serveur GIT pour le téléchargement et l'importation du code de source. Les référentiels doivent se trouver à `/sie/*` sur ce serveur. |
| `USE_CACHE`           | `0`                         | Mettre `1` ici pour activer le cache de Superset pour son accélération. *Ne jamais mettre `1` ici pour un système de développement !*             |

Certaines autres configurations peuvent être effectuées dans les répertoires racine des différents conteneurs. En particulier, on peut ajouter des données initiales pour les conteneurs `ldap-server`, `postgresql` et `superset`.

## Lancer le service

Le système du SIE demande comme fondation un serveur Linux. Les instructions dans ce document sont dirigées vers un serveur Ubuntu Server 18.04 mais en principe tout autre système d’exploitation peut être utilisé en adaptant les commandes.

Les seuls paquets additionnels qu’il faut installer sont *docker.io*, *docker-compose* et *git*. L’installation de ces paquets se fait par le commande : `apt update && apt install -y docker.io docker-compose git`.

Après, on peut télécharger les sources du système avec le commande :

```sh
git clone https://gitea.energie.mg/sie/shell_docker.git && cd shell_docker
```

Le service est lancé par le commande

```sh
docker-compose up -d
```

exécuté dans le dossier racine (ou se trouve le fichier `docker-compose.yml`). *Docker compose* va ensuite générer et lancer les différents conteneurs.

Docker relancera automatiquement tous les conteneurs après un redémarrage du système hôte.

# Dépannage

Quand le système échoue, Il faut d'abord trouver la raison pour la panne et après la résoudre. À cause de la complexité du système il existe une grande nombre de raisons possibles et certaines catégories de pannes ne peuvent être résolues qu'avec une compréhension profonde des différentes technologies employées. Quand même, le système a été composé de manière à minimiser les risques de telles pannes, par exemple par l'isolation des composantes du système en conteneurs Docker.

De l'autre côté, il-y-a aussi des autres classes de pannes lesquelles peuvent toujours arriver et lesquelles peuvent être résolues avec une compréhension fondamentale des systèmes numériques interconnectés. Vous trouvez ici une liste de pannes possibles avec des suggestions pour leurs résolutions. Dans les exemples, « sie-madagascar.info » est utilisé comme nom de domaine pour le système. Remplacez-le avec le nom correct.

## Système inaccessible

Cette situation peut arriver à cause d'une grande nombre de différentes raisons. Si la raison n'est pas encore connue, testez pour eux dans l'ordre suivant :

### Raison : Erreur de DNS

**Test** : Essayez de trouver l'adresse IP du système à partir d'un outil client DNS, par ex. : `nslookup sie-madagascar.info`. S'il-y-a une réponse, validez que l'adresse retournée est correcte.

**Résolution** : Contactez le fournisseur du domaine.

### Raison : Erreur de connexion

**Test** : Lancez `ping sie-madagascar.info`

**Résolution** : Contactez le fournisseur du serveur. Il-y-a plusieurs raisons possibles derrière le problème, comme :

* Il-y-a un problème chez le fournisseur
* Le système s'est écrasé

La résolution finale dépende de cette cause fondamentale, mais normalement ce sera le fournisseur du serveur qui peut réparer la connexion ou redémarrer le système.

### Raison : Erreur de certificat

**Test** : Accédez au site avec votre navigateur → le navigateur montre un message d'erreur concernant le certificat de cryptage.

**Résolution** : Essayez de recréer le certificat. Sur le serveur, exécutez dans le dossier d'installation du système les commandes suivantes :

```{sh}
docker stop sie_shell_1
docker rm sie_shell_1
docker volume rm sie_cert-storage
docker-compose up -d
```

*Attention* : Le certificat est fourni par « Let's Encrypt ». Ces certificats sont gratuits, mais il-y-a certaines [limites](https://letsencrypt.org/fr/docs/rate-limits/) employées par leur serveur. Par ex., on ne peut pas demander plus de 50 certificats par semaine et il n'est pas possible de lancer le processus décrit ci-dessus plus que 10 fois en 3 heures.

### Raison : Service HTTP indisponible

**Test** : Accédez au site avec votre navigateur.

**Résolution** : Relancez les composantes du système par exécution de `docker-compose restart -d` dans le dossier d'installation sur le serveur.
